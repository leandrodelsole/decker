import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

export default class CardText extends Component {
    render() {
        const ctClassName = classnames({
            'panel-primary': this.props.cover,
            'panel-danger': !this.props.cover,
            'panel': true,
        });

        return (
            <div className={ctClassName}>
                <div className="panel-heading">
                    <h3 className="panel-title">
                        Card theme - <strong>{this.props.card.username}</strong>
                        <button className="card-delete" onClick={this.props.delete.bind(this)}>
                            &times;
                        </button>
                    </h3>
                </div>
                <div className="panel-body">
                    Q: {this.props.card.cover}
                </div>
            </div>
        );
    }
}

CardText.propTypes = {
    card: PropTypes.object.isRequired,
    delete: PropTypes.func.isRequired,
    cover: PropTypes.bool.isRequired,
};