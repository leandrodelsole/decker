import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import CardText from './cards/CardText.jsx';

export default class Card extends Component {
    delete() {
        Meteor.call('cards.remove', this.props.card._id);
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-6">
                    <CardText card={this.props.card} delete={this.delete} cover={true} />
                </div>
                <div className="col-sm-6">
                    <CardText card={this.props.card} delete={this.delete} cover={false} />
                 </div>
            </div>
        );
    }
}

Card.propTypes = {
    card: PropTypes.object.isRequired,
};