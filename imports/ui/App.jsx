import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { createContainer } from 'meteor/react-meteor-data';

import { Cards } from '../api/cards.js';

import Card from './Card.jsx';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';

// App component - represents the whole app
class App extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }
    
    renderCards() {
        return this.props.cards.map((card) => (
            <Card key={card._id} card={card} />
        ));
    }

    handleSubmit(event) {
        event.preventDefault();

        // Find the text field via the React ref
        const textCoverInput = ReactDOM.findDOMNode(this.refs.textCover);
        const textCover = textCoverInput.value.trim();

        const textContentInput = ReactDOM.findDOMNode(this.refs.textContent);
        const textContent = textContentInput.value.trim();

        Meteor.call('cards.insert', textCover, textContent);

        // Clear form
        textCoverInput.value = '';
        textContentInput.value = '';
    }

    render() {
        return (
            <div className="container">
                <header>
                    <h1 className="title">Decker</h1>
                    <p>use your deck to test yourself or anyone else</p>
                </header>

                <AccountsUIWrapper />

                { this.props.currentUser ?
                    <form className="new-card" onSubmit={this.handleSubmit.bind(this)} >
                        <input
                            type="text"
                            ref="textCover"
                            placeholder="Type the card cover text"
                        />
                        <input
                            type="text"
                            ref="textContent"
                            placeholder="Type the card content text"
                        />
                        <button type="submit" className="btn btn-default">
                            <span className="glyphicon glyphicon-plus-sign" aria-hidden="true" />
                        </button>
                    </form> : ''
                }

                {this.renderCards()}
            </div>
        );
    }
}

App.propTypes = {
    cards: PropTypes.array.isRequired,
    currentUser: PropTypes.object,
};

export default createContainer(() => {
    Meteor.subscribe('cards');

    return {
        cards: Cards.find({}, { sort: { createdAt: -1 } }).fetch(),
        currentUser: Meteor.user(),
    };
}, App);