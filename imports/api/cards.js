import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Cards = new Mongo.Collection('cards');

if (Meteor.isServer) {
    Meteor.publish('cards', function cardsPublication() {
        return Cards.find({ owner: this.userId }, { sort: { createdAt: -1 } });
    });
}

Meteor.methods({
    'cards.insert'(cover, content = 'content') {
        check(cover, String);
        check(content, String);

        // Make sure the user is logged in before inserting a task
        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        Cards.insert({
            cover,
            content,
            createdAt: new Date(),
            owner: this.userId,
            username: Meteor.users.findOne(this.userId).username,
        });
    },
    'cards.remove'(cardId) {
        check(cardId, String);

        const card = Cards.findOne(cardId);
        if (card.owner !== this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        Cards.remove(cardId);
    },
});